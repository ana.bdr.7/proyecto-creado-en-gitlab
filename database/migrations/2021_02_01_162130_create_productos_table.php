<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug');
            $table->string('titulo');
            $table->time('duracion');
            $table->time('horario');
            $table->date('fecha');
            $table->float('precio',8,2);
            $table->text('objetivos');            
            $table->text('descripcion');
            $table->string('imagen');
            $table->string('ubicacion');
            $table->integer('level_id')->unsigned(); 
            $table->foreign('level_id')->references('id')->on('levels');
            $table->integer('categoria_id')->unsigned(); 
            $table->foreign('categoria_id')->references('id')->on('categorias');
            $table->integer('user_id')->unsigned(); 
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productos');
    }
}
