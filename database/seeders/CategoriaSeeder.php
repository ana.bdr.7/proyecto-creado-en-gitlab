<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Categoria;
use Illuminate\Support\Str;

class CategoriaSeeder extends Seeder
{
    private $categorias = array(
        array(
        'tipo' => 'actividad'
    ),array(
        'tipo' => 'curso'
    ));
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach($this->categorias as $categoria){
            $a = new Categoria();
            $a->tipo = $categoria['tipo'];
            $a->save();
        }
        $this->command->info('Tabla categorias inicializada con datos');
    }
}
