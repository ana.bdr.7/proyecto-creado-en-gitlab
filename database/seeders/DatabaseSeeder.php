<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Database\Seeders\ProductoSeeder;
use Database\Seeders\RolSeeder;
use Database\Seeders\UserSeeder;
use Database\Seeders\LevelSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //limpieza tablas
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('reservas')->truncate();
        DB::table('productos')->truncate();
        DB::table('users')->truncate();
        DB::table('categorias')->truncate();
        DB::table('levels')->truncate();
        DB::table('rols')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        DB::table('categorias')->delete();         
        $this->call(CategoriaSeeder::class);

        DB::table('rols')->delete();         
        $this->call(RolSeeder::class);

        DB::table('levels')->delete();         
        $this->call(LevelSeeder::class);

        DB::table('users')->delete();         
        $this->call(UserSeeder::class);             
        
        DB::table('productos')->delete();         
        $this->call(ProductoSeeder::class);
        
        
    }
}
