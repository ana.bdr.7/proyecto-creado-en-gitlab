<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Producto;
use Illuminate\Support\Str;

class ProductoSeeder extends Seeder
{

    private $productos = array(
		array(
            'slug' => 'curso_basico_alpinismo_invernal',
			'titulo' => 'curso basico alpinismo invernal',            
			'duracion' => '02:00:00',
			'horario' => '09:00',
			'fecha' => '2021-03-22',
            'precio' => '150',
            'imagen' => 'alpinismo-material.jpeg',
			'objetivos' => 'acceso con crampones y piolet basico',
            
            'descripcion' => 'El curso de alpinismo básico es la mejor puerta de entrada al fascinante mundo de la montaña invernal; descubrirás técnicas y conocimientos para que disfrutes con seguridad de este deporte. Tan solo necesitarás ropa adecuada para la nieve y unas botas cramponables',
            'ubicacion' => 'Picos de Europa',
            'level_id'=> 1,
            'categoria_id' => 1,
            'user_id' => 1
        ),array(
            'slug' => 'curso_intermedio_alpinismo_invernal',
            'titulo' => 'curso intermedio alpinismo invernal',            
            'duracion' => '08:00:00',
            'horario' => '09:00',
            'fecha' => '2021-03-29',
            'precio' => 350,
            'imagen' => 'alpinismo-material.jpeg',
            'objetivos' => ' Caminar por laderas nevadas o ascender',
         
            'descripcion' => 'Para realizar el curso de alpinismo intermedio es necesario tener los conocimientos adquiridos en el nivel básico (manejo mínimo de piolet, crampones y autodetenciones).',
            'ubicacion' => 'Picos de Europa',
            'level_id'=> 2,
            'categoria_id' => 2,
            'user_id' => 1
        ),
        array(
            'slug' => 'curso_avanzado_alpinismo_invernal',
            'titulo' => 'curso avanzado alpinismo invernal',            
            'duracion' => '08:00:00',
            'horario' => '09:00',
            'fecha' => '2021-03-29',
            'precio' => 350,
            'imagen' => 'alpinismo-material.jpeg',
            'objetivos' => 'La nieve y el hielo se acumulan de forma caprichosa, dando lugar a palas, canales y corredores, cuya escalada requiere de conocimientos técnicos en nieve-hielo y roca, obligándonos a conocer los distintos sistemas de aseguramiento para cada tipo de terreno.',
            
            'descripcion' => 'Para realizar el curso de alpinismo avanzado es necesario tener los conocimientos adquiridos en el nivel básico e intermedio. Para un mejor aprovechamiento de las clases, es muy recomendable nuestro monográfico de Escalada Iniciación.',
            'ubicacion' => 'Picos de Europa',
            'level_id'=> 2,
            'categoria_id' => 1,
            'user_id' => 1
        ),
        array(
            'slug' => 'escalada_deportiva_iniciacion',
            'titulo' => 'escalada deportiva iniciación',            
            'duracion' => '08:00:00',
            'horario' => '09:00',
            'fecha' => '2021-03-29',
            'precio' => 350,
            'imagen' => 'alpinismo-material.jpeg',
            'objetivos' => 'iniciación en la escalada en roca de vías de un largo equipadas.',
            
            'descripcion' => 'Este curso de escalada iniciación es, sin duda, la mejor puerta de entrada al fascinante mundo de la escalada; descubrirás técnicas y conocimientos para que disfrutes con seguridad de este deporte.',
            'ubicacion' => 'Picos de Europa',
            'level_id'=> 2,
            'categoria_id' => 2,
            'user_id' => 1
        ),
        array(
            'slug' => 'escalada_rocodromo_iniciacion',
            'titulo' => 'escalada rocodromo iniciación',            
            'duracion' => '08:00:00',
            'horario' => '09:00',
            'fecha' => '2021-03-29',
            'precio' => 350,
            'imagen' => 'alpinismo-material.jpeg',
            'objetivos' => ' días para aprender de forma rápida y eficaz los fundamentos básicos de la escalada en nuestro rocódromo ROC 30: técnica gestual, porteos, material de seguridad y aseguramiento con cuerda...',
           
            'descripcion' => 'Este curso de iniciación al rocódromo es, sin duda, la mejor puerta de entrada al fascinante mundo de la escalada.',
            'ubicacion' => 'Picos de Europa',
            'level_id'=> 2,
            'categoria_id' => 1,
            'user_id' => 1
        ),
        array(
            'slug' => 'escalada_deportiva_varios_largos',
            'titulo' => 'escalada deportiva varios largos',            
            'duracion' => '08:00:00',            
            'horario' => '09:00',
            'fecha' => '2021-03-29',
            'precio' => 350,
            'imagen' => 'alpinismo-material.jpeg',
            'objetivos' => ' Comenzar a trepar vías de varios largos debe hacerse de forma progresiva con el objetivo de no mermar la seguridad de la cordada.',
            
            'descripcion' => 'Este curso en vías equipadas requiere haber realizado el Nivel I de Escalada o bien saber escalar de primero de cordada con soltura en vías equipadas de un largo y de una dificultad mínima de V+',
            'ubicacion' => 'Picos de Europa',
            'level_id'=> 2,
            'categoria_id' => 2,
            'user_id' => 1
        ),
        array(
            'slug' => 'escalada_clasica_via_semiequipada',
            'titulo' => 'escalada clasica via semiequipada',           
            'duracion' => '08:00:00',
            'horario' => '09:00',
            'fecha' => '2021-03-29',
            'precio' => 350,
            'imagen' => 'alpinismo-material.jpeg',
            'objetivos' => ' El empleo de anclajes móviles y el aprendizaje de maniobras complementarias de seguridad para la cordada, te permitira afrontar itinerarios de mayor envergadura.',
        
            'descripcion' => 'Para sacar todo el "jugo" a este curso necesitamos que tengas conocimientos previos de escalada deportiva de primero y rápel con solvencia. Nivel de escalada requerido 5+',
            'ubicacion' => 'Picos de Europa',
            'level_id'=> 2,
            'categoria_id' => 2,
            'user_id' => 1
        ),
        array(
            'slug' => 'escalada_clasica_via_desequipada',
            'titulo' => 'escalada clasica via desequipada',            
            'duracion' => '08:00:00',
            'horario' => '09:00',
            'fecha' => '2021-03-29',
            'precio' => 350,
            'imagen' => 'alpinismo-material.jpeg',
            'objetivos' => ' El empleo de anclajes móviles y el aprendizaje de maniobras complementarias de seguridad para la cordada, te permitira afrontar itinerarios de mayor envergadura.',
           
            'descripcion' => 'Para sacar todo el "jugo" a este curso necesitamos que tengas conocimientos previos de escalada deportiva de primero y rápel con solvencia. Nivel de escalada requerido 5+',
            'ubicacion' => 'Picos de Europa',
            'level_id'=> 2,
            'categoria_id' => 1,
            'user_id' => 1
        ),
        );
   /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       
        foreach($this->productos as $producto){
            $a = new Producto();
            $a->slug = $producto['slug'];
            $a->titulo = $producto['titulo'];
            $a->duracion = $producto['duracion'];
            $a->horario = $producto['horario'];
            $a->fecha = $producto['fecha'];
            $a->precio = $producto['precio'];
            $a->imagen = $producto['imagen'];
            $a->objetivos = $producto['objetivos'];
            $a->level_id = $producto['level_id'];
            $a->descripcion = $producto['descripcion']; 
            $a->ubicacion = $producto['ubicacion'];
            $a->categoria_id = $producto['categoria_id'];
            $a->user_id = $producto['user_id'];           
            $a->save();
        }
        $this->command->info('Tabla productos inicializada con datos');
    }
}
