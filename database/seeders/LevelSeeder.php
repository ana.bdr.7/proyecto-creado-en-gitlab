<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Level;
use Illuminate\Support\Str;

class LevelSeeder extends Seeder
{
    private $levels = array(
        array(
            'nombre' => 'Principiante'
        ),
        array(
            'nombre' => 'Avanzado'
        ),
        array(
            'nombre' => 'Profesional'
        )
        );
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach($this->levels as $level){
            $a = new Level();
            $a->nombre = $level['nombre'];
            $a->save();
        }
        $this->command->info('Tabla levels rellenada con datos');
    }
}
