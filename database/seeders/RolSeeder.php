<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Rol;
use Illuminate\Support\Str;

class RolSeeder extends Seeder
{
    private $roles = array(
        array(
            'tipo' => 'cliente'
        ),
        array(
            'tipo' => 'empresa'
        )
        );
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach($this->roles as $rol){
            $a = new Rol();
            $a->tipo = $rol['tipo'];
            $a->save();
        }
        $this->command->info('Tabla roles rellenada con datos');
    }
}
