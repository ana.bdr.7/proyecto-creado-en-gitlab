<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;


class UserSeeder extends Seeder
{
    /*private $users = array(
        array(
            'name' => 'admin',
            'email' => 'admin@gmail.com',
            'password' => 'admin'
        )
        );*/
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
       
        DB::table('users')->insert([
            'name' => 'admin',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('admin'),
            'rol_id' => 2
        ]);

        DB::table('users')->insert([
            'name' => 'pepe',
            'email' => 'pepe@gmail.com',
            'password' => bcrypt('pepe'),
            'rol_id' => 1
        ]);
        $this->command->info('Tabla usuarios inicializada con datos');
    }
}
