@extends('layouts.master') 
 
@section('titulo')
  Zoológico 
@endsection 
 
@section('contenido')
<div class="container">
    <div class="row">
        @foreach($productos as $clave => $producto)
            <div class="col-sm-12 col-md-4">        
                <div class="card" style="width: 18rem;">
                    <img src="{{asset('/assets/img/' .$producto->imagen)}}"/>
                        <div class="card-body">
                            <h5 class="card-title">{{$producto->titulo}}</h5>
                            <p class="card-text">{{$value = Str::limit($producto->descripcion, 123)}}</p>
                            <a href="{{ route('productos.detalle.show' , $producto->slug ) }}" class="btn btn-success"><i class="fas fa-plus"></i> info</a>
                        </div>
                 </div>   
            </div>
        @endforeach
     </div>

  <div class="row">
    <div class="col-sm">
    {!! $productos->links() !!}
    </div>
  </div>
</div>
@endsection 