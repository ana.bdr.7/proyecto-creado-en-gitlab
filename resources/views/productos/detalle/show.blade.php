@extends('layouts.master') 
 
@section('titulo')
  Zoológico 
@endsection 
 
@section('contenido')
@if(session('mensaje'))
<div class="alert alert-info">
    {{ session('mensaje') }}
</div>
@endif
<div class="row"> 
    <div class="col-sm-3"> 
        <img class="imagen" src="{{asset('/assets/img/' .$producto->imagen)}}">
    </div> 
    <div class="col-sm-9"> 
    
       <p><strong>{{$producto->titulo}}</strong></p>
       <p><strong>Duracion: </strong>{{$producto->duracion}}</p>
       <p><strong>Precio: </strong>{{$producto->precio}}</p>
       <p><strong>Ubicacion: </strong>{{$producto->ubicacion}}</p>       
       <p><strong>Horario: </strong>{{$producto->horario}}</p> 
       <p><strong>Descripcion: </strong>{{$producto->descripcion}}</p>
       <a href="{{ route('productos.reserva' , $producto->slug ) }}" class="btn btn-success">Reserva</a>

       
    </div> 
</div>  
@endsection 