@extends('layouts.master') 
 
@section('titulo')
  Zoológico 
@endsection 
 
@section('contenido')
    <div class="container">
        <div class="row">
            <div class="col md-12">
                <h2>Estas a punto de hacer una reserva de {{$producto->titulo}}</h2>
                <p>Por favor, indicanos el número de personas que vais a participar</p>
                <form method="post" action= "{{ route('reserva.add') }}">             
                    @csrf              
                    <input type="number" name="personas" id="personas" min=1>
                    <input type="hidden" name="producto" value="{{$producto->id}}">
                    <input type="submit" class="btn btn-success" value="Finalizar Reserva">
                <form>
            </div>
        </div>
    </div>
  

@endsection 