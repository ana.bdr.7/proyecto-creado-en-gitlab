@extends('layouts.master') 
 
@section('titulo')
  Zoológico 
@endsection 
 
@section('contenido')
  <div class="container-sm">
    <div class="list-group">
        <a href="{{ route('admin.profile') }}" class="list-group-item list-group-item-action">Mis Datos</a>        
        <a href="{{ route('admin.add') }}" class="list-group-item list-group-item-action">Añadir Productos</a>
        <a href="{{ route('admin.misproductos') }}" class="list-group-item list-group-item-action active" aria-current="true">Mis Productos</a>
        <a href="{{ route('admin.reserva') }}" class="list-group-item list-group-item-action">Calendario</a>
    </div>
  </div>
  @if(session('mensaje'))
<div class="alert alert-info">
    {{ session('mensaje') }}
</div>
@endif
<div class="container">
    <div class="row">
        @foreach($productos as $clave => $producto)
            <div class="col-sm-12 col-md-4">        
                <div class="card" style="width: 18rem;">
                    <img src="{{asset('/assets/img/' .$producto->imagen)}}"/>
                        <div class="card-body">
                            <h5 class="card-title">{{$producto->titulo}}</h5>
                            <p class="card-text">{{$value = Str::limit($producto->descripcion, 123)}}</p>
                            <div class="btn-group" role="group" aria-label="Basic example">
                                <a role="button" href="{{ route('admin.edit' , $producto->slug ) }}" class="btn btn-success"><i class="fas fa-pencil-alt"></i></a>
                                <a role="button" href="{{ route('admin.delete' , $producto->id ) }}" class="btn btn-success"><i class="fas fa-trash-alt"></i></a>                                                   
                            </div>
                            
                        </div>
                 </div>   
            </div>
        @endforeach
     </div>

 
  <div class="row">
    <div class="col-sm">
    {!! $productos->links() !!}
    </div>
  </div>
</div>

@endsection