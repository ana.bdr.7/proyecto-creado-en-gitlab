@extends('layouts.master') 
 
@section('titulo')
  Zoológico 
@endsection 
 
@section('contenido')
  <div class="container-sm">
    <div class="list-group">
        <a href="{{ route('admin.profile') }}" class="list-group-item list-group-item-action active" aria-current="true">Mis Datos</a>        
        <a href="{{ route('admin.add') }}" class="list-group-item list-group-item-action">Añadir Productos</a>
        <a href="{{ route('admin.misproductos') }}" class="list-group-item list-group-item-action">Mis Productos</a>
        <a href="{{ route('admin.reserva') }}" class="list-group-item list-group-item-action">Calendario</a>
    </div>
  </div>
  <div class="container">
      <p><strong>{{$user->name}}</strong></p>
      <p><strong>Duracion: </strong>{{$user->email}}</p>
  </div>
  @section('contenido')
<div class="container">
    <div class="row">
        @foreach($productos as $clave => $producto)
            <div class="col-sm-12 col-md-4">        
                <div class="card" style="width: 18rem;">
                    <img src="{{asset('/assets/img/' .$producto->imagen)}}"/>
                        <div class="card-body">
                            <h5 class="card-title">{{$producto->titulo}}</h5>
                            <p class="card-text">{{$value = Str::limit($producto->descripcion, 123)}}</p>
                            <a href="{{ route('admin.edit' , $producto->id ) }}" class="btn btn-success">Modificar</a>
                        </div>
                 </div>   
            </div>
        @endforeach
     </div>

  <div class="row">
    <div class="col-sm">
    <nav aria-label="Page navigation example">
  <ul class="pagination justify-content-center">
    <li class="page-item">
      <a class="page-link" href="#" aria-label="Previous">
        <span aria-hidden="true">&laquo;</span>
      </a>
    </li>
    <li class="page-item"><a class="page-link" href="#">1</a></li>
    <li class="page-item"><a class="page-link" href="#">2</a></li>
    <li class="page-item"><a class="page-link" href="#">3</a></li>
    <li class="page-item">
      <a class="page-link" href="#" aria-label="Next">
        <span aria-hidden="true">&raquo;</span>
      </a>
    </li>
  </ul>
</nav>
    </div>
  </div>
</div>
@endsection 
@endsection 