@extends('layouts.master') 
 
@section('titulo')
  Zoológico 
@endsection 
 
@section('contenido')
@if(session('mensaje'))
<div class="alert alert-info">
    {{ session('mensaje') }}
</div>
@endif
  <div class="container-sm">
    <div class="list-group">
        <a href="{{ route('admin.profile') }}" class="list-group-item list-group-item-action active" aria-current="true">Mis Datos</a> 
        @if($user->rol_id == 2)       
        <a href="{{ route('admin.add') }}" class="list-group-item list-group-item-action">Añadir Productos</a>
        <a href="{{ route('admin.misproductos') }}" class="list-group-item list-group-item-action">Mis Productos</a>
        @endif
        <a href="{{ route('admin.reserva') }}" class="list-group-item list-group-item-action">Calendario</a>
    </div>
  </div>
  <div class="container">
    <div class="card text-white bg-success mb-3" style="max-width: 18rem;">
      <div class="card-header">Mis Datos Personales</div>
      <div class="card-body">
        <h5 class="card-title">Nombre</h5>
        <p class="card-text">{{$user->name}}</p>
        <h5 class="card-title">Email</h5>
        <p class="card-text">{{$user->email}}</p>
    </div>
  </div>
 
@endsection



  