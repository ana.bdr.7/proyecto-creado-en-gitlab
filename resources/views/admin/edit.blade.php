@extends('layouts.master')

@section('titulo')
    zoologico
@endsection

@section('contenido')
@section('contenido')
  <div class="container-sm">
    <div class="list-group">
        <a href="{{ route('admin.profile') }}" class="list-group-item list-group-item-action">Mis Datos</a>        
        <a href="{{ route('admin.add') }}" class="list-group-item list-group-item-action">Añadir Productos</a>
        <a href="{{ route('admin.misproductos') }}" class="list-group-item list-group-item-action">Mis Productos</a>
        <a href="{{ route('admin.reserva') }}" class="list-group-item list-group-item-action">Calendario</a>
    </div>
  </div>
  @if(session('mensaje'))
<div class="alert alert-info">
    {{ session('mensaje') }}
</div>
@endif
<div class="row">     
    <div class="offset-md-3 col-md-6">        
        <div class="card">           
            <div class="card-header text-center">             
                 Editar animal           
            </div>           
            <div class="card-body" style="padding:30px"> 
 
            <form method="post" action="{{ route('admin.update',$producto->slug) }}" enctype="multipart/form-data">             
                    @csrf
                    @method('put')              
                    <div class="form-group">                 
                        <label for="titulo">Titulo</label>                 
                        <input type="text" name="titulo" id="titulo" class="form-control" value="{{$producto->titulo}}" required>              
                    </div> 

                    <div class="form-group">
                        <label for="duracion">Duración</label>                 
                        <input type="time" name="duracion" id="duracion" class="form-control"  value="{{$producto->duracion}}" required>             
                    </div> 

                    <div class="form-group">
                        <label for="horario">Horario</label>                  
                        <input type="time" name="horario" id="horario" class="form-control"  value="{{$producto->horario}}" required>              
                    </div> 

                    <div class="form-group">
                        <label for="fecha">Fecha</label>                  
                        <input type="date" name="fecha" id="fecha" class="form-control" value="{{$producto->fecha}}" required>              
                    </div> 

                    <div class="form-group">
                        <label for="precio">Precio</label>                 
                        <input type="number" name="precio" id="precio" class="form-control" value="{{$producto->precio}}" required>             
                    </div> 

                    <div class="form-group">  
                        <label for="objetivos">Objetivos</label>               
                        <textarea name="objetivos" id="objetivos" class="form-control" required> {{$producto->objetivos}} </textarea>                
                    </div> 

                    <div class="form-group">                                         
                        <label for="descripcion">Descripcion</label>                 
                        <textarea name="descripcion" id="descripcion" class="form-control" rows="3">{{$producto->descripcion}}</textarea>           
                    </div> 

                    <div class="form-group">
                        <label for="ubicacion">Ubicacion</label>                  
                        <input type="text" name="ubicacion" id="ubicacion" class="form-control" value="{{$producto->ubicacion}}" required>         
                    </div>


                    <div class="form-group">
                        <label for="categoria_id">Categoria</label>  
                        
                        <select class="form-select" aria-label="Default select example" name="categoria_id">
                            @foreach($categorias as $categoria)
                        
                                <option value="{{$categoria->id}}">{{$categoria->tipo}}</option>

                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="level_id">Dificultad</label>  
                        
                        <select class="form-select" aria-label="Default select example" name="level_id">                          
                        
                                <@foreach($levels as $level)
                        
                                <option value="{{$level->id}}">{{$level->nombre}}</option>

                                 @endforeach
                            
                        </select>
                    </div>
                    

                    <div class="form-group"> 
                        <label for="imagen">Imagen</label>                                         
                        <input type="file" name="imagen" id="imagen" class="form-control" required>             
                    </div>
                    <div class="form-group text-center">                
                        <button type="submit" class="btn btn-success" style="padding:8px 100px;margin-top:25px;">             
                            Modificar                
                        </button>    
                    </div>          
                </form>
            </div>       
        </div>     
    </div>  
</div>
@endsection