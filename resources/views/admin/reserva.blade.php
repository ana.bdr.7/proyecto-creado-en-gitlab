@extends('layouts.master') 
 
@section('titulo')
  Zoológico 
@endsection 
 
@section('contenido')

<!--falta el menu al lado-->
<!--modificar el diseño-->
<div class="container">
  <div class="row">
    <div class="col md-12">
      @if(count($reservas)>1)

@foreach($reservas as  $reserva)
<div class="card">
  <h5 class="card-header">{{$reserva->titulo}}</h5>
  <div class="card-body">
  <p class="card-text"><strong>Horario</strong></p>    
    <p class="card-text">{{$reserva->horario}}</p>
    <p class="card-text"><strong>Fecha</strong></p>
    <p class="card-text">{{$reserva->fecha}}</p>
    <p class="card-text"><strong>Ubicacion</strong></p>
    <p class="card-text">{{$reserva->ubicacion}}</p>
    <p class="card-text"><strong>Precio</strong></p>
    <p class="card-text">{{$reserva->precio}}€</p>
    <p class="card-text"><strong>Duracion</strong></p>
    <p class="card-text">{{$reserva->duracion}}</p>
    
  </div>
</div>


@endforeach
@else
<div class="alert alert-success" role="alert">
  No tienes ninguna reserva
</div>
@endif
    </div>
  </div>
</div>
@endsection