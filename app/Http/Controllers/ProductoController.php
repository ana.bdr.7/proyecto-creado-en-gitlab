<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\InicioController;
use Illuminate\Support\Facades\DB;
use App\Models\Producto;
use App\Models\Categoria;
use App\Models\Level;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

class ProductoController extends Controller
{
    public function show($slug){

        $producto = DB::table('productos')->where('slug',$slug)->first();

        return view('productos.detalle.show',['producto' => $producto]);
    }

    public function index(){

        $productos = Producto::all();

        return view('productos.index',['productos' => $productos]);
    }
    
    public function my_products(){

        $productos = Producto::paginate(6);

        return view('admin.misproductos',['productos' => $productos]);
    }

    public function create(){

        $categorias = Categoria::all();
        $levels = Level::all();

        
		
        return view('admin.add',['categorias' => $categorias, 'levels' => $levels]);
    }

    public function edit($slug){

        

        $producto = DB::table('productos')->where('slug',$slug)->first();

        $categorias = Categoria::all();
        $levels = Level::all();

       

     
		
		
        return view('admin.edit', ['producto' => $producto,'categorias' => $categorias, 'levels' => $levels]);
	}

    public function getByCategoryId($categoria_id = null){

        $productos = DB::table('productos')->where('categoria_id', $categoria_id)->paginate(6);
        
        return view('productos.index',['productos' => $productos]);
    }

    public function store(Request $request){

       
        
        $datos = $request->except('_token');
        $datos['slug'] = Str::slug($request->titulo);
        $datos['imagen'] = $request->imagen->store('','productos');
        $datos['user_id'] = $request->user()->id;

        
        try{
            $producto = Producto::create($datos);
            return redirect()->route('admin.add')->with('mensaje','Animal subido correctamente');
        }catch(Illuminate\Database\QueryException $ex){            
            return redirect()->route('admin.add')->with('mensaje','Fallo al subir el animal');
        }
            
       
    }

    public function update(Request $request,Producto $producto){

        $producto->titulo = $request->titulo;
        $producto->slug = Str::slug($request->titulo);
        $producto->duracion = $request->duracion;
        $producto->horario = $request->horario;
        $producto->fecha = $request->fecha;
        $producto->precio = $request->precio;
        $producto->objetivos = $request->objetivos;
        $producto->descripcion = $request->descripcion;
        $producto->ubicacion = $request->ubicacion;
        $producto->categoria_id = $request->categoria_id;
        $producto->level_id = $request->level_id;
        $producto->user_id = $request->user()->id;
        
        
        if(!empty($request->imagen)){
            Storage::disk('productos')->delete($producto->imagen);
            $producto->imagen = $request->imagen->store('','productos');
        }
        $producto->save();
        
        
        return redirect()->route('admin.misproductos',$producto->id);
}

    public function delete($id){

        $producto = Producto::find($id);
        
        $producto->delete();

        return redirect()->route('admin.misproductos')->with("Producto borrado correctamente");

    }

    public function reserva($slug){


        $producto = DB::table('productos')->where('slug',$slug)->first();


        return view('productos.reserva',['producto' => $producto]);

    }
}
