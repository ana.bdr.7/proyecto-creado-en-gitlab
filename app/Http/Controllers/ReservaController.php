<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Reserva;
use Illuminate\Support\Facades\DB;

class ReservaController extends Controller
{
    public function add(Request $request){

       
        $reserva['producto_id'] = $request->producto;
        $reserva['personas'] = $request->personas;
        $reserva['user_id'] = $request->user()->id;

        
        try{
            $reserva = Reserva::create($reserva);
            return redirect()->route('admin.index')->with('mensaje','Reserva realizada correctamente');
        }catch(Illuminate\Database\QueryException $ex){            
            return redirect()->route('productos.index')->with('mensaje','Fallo al realizar la reserva');
        }
            
    }

    public function mis_reservas(Request $request){

        $user_id = $request->user()->id;

        $reservas = DB::table('reservas')
            ->join('productos', 'productos.id', '=', 'reservas.producto_id')           
            ->where('reservas.user_id',$user_id)->get();
            

        return view('admin.reserva',['reservas' => $reservas]);
    }
}
