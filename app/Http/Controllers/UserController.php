<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\InicioController;
use App\Http\Controllers\ProductoController;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    public function infoprofesional(Request $request){
        //dd($request->user());


        //$info = DB::table('users')->where('id',$id)->first();
        $user = $request->user();
        return view('admin.profile',['user' => $user]);
    }
}
