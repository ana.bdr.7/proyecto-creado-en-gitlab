<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\ProductoController;
use Illuminate\Support\Facades\DB;
use App\Models\Categoria;


class InicioController extends Controller
{
    public function inicio(){

        $categorias = DB::table('categorias')->get();
     

        //return redirect()->action([ProductoController::class,'/']);
        return view('inicio', ['categorias'=>$categorias]);
    }
}
