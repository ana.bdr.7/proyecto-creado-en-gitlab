<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ProductoController;

class Reserva extends Model
{
    protected $guarded = [];
    use HasFactory;

    public function productos()
    {
        return $this->belongsTo(Producto::class);
    }

    public function users()
    {
        return $this->belongsTo(User::class);
    }
}
