<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\ProductoController;

class Level extends Model
{
    protected $guarded = [];
    use HasFactory;

    public function productos()
    {
        return $this->hasMany(Producto::class);
    }
}
