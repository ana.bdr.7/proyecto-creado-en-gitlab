<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\UserController;

class Rol extends Model
{
    protected $guarded = [];
    use HasFactory;


    public function users()
    {
        return $this->hasMany(User::class);
    }
}
