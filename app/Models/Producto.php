<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\UserController;
use App\Http\Controllers\CategoriaController;
use App\Http\Controllers\LevelController;

class Producto extends Model
{
    protected $guarded = [];
    use HasFactory;


    public function users()
    {
        return $this->belongsTo(User::class);
    }

    public function categorias()
    {
        return $this->belongsTo(Categoria::class);
    }

    public function levels()
    {
        return $this->belongsTo(Level::class);
    }
}
