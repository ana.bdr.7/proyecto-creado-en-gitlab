<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductoController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ReservaController;
use App\Http\Controllers\InicioController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', [InicioController::class, 'inicio'])->name('home');

Route::get("productos/index", [ProductoController::class, 'index'])->name('productos.index');

Route::get("admin/misproductos", [ProductoController::class, 'my_products'])
    ->name('admin.misproductos')
    ->middleware('auth');



Route::get("productos/{categoria}", [ProductoController::class , 'getByCategoryId'])->name('productos.categoria');

Route::get("productos/detalle/{producto}", [ProductoController::class, 'show'])->name('productos.detalle.show');

Route::get("productos/reserva/{producto}", [ProductoController::class, 'reserva'])
    ->name('productos.reserva')
    ->middleware('auth');

Route::post("reservas/add" ,[ReservaController::class , 'add'])
    ->name('reserva.add')
    ->middleware('auth');

Route::get("admin/profile", [UserController::class, 'infoprofesional'])
    ->name('admin.profile')
    ->middleware('auth');

Route::get("admin/delete/{producto}", [ProductoController::class, 'delete'])
    ->name('admin.delete')
    ->middleware('auth');

Route::get("admin/add", [ProductoController::class, 'create'])
    ->name('admin.add')
    ->middleware('auth');

Route::post("admin/add", [ProductoController::class, 'store'])
    ->name('admin.store')
    ->middleware('auth');

Route::get("admin/{producto}/edit",[ProductoController::class, 'edit'])
    ->name('admin.edit')
    ->middleware('auth');

Route::put("admin/perfilprofesional",[ProductoController::class, 'update'])
    ->name('admin.update')
    ->middleware('auth');

Route::get("admin/reserva", [ReservaController::class, 'mis_reservas'])
    ->name('admin.reserva')
    ->middleware('auth');

Route::middleware(['auth:sanctum', 'verified'])->get('/profile', function () {
    return view('dashboard');
})->name('dashboard');
